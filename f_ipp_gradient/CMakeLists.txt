# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

# Set the name of the project as the directory basename
project(f_ipp_gradient)

find_package(blc_channel)
find_package(blc_program)

add_definitions(${BL_DEFINITIONS}  )
include_directories(${BL_INCLUDE_DIRS} $ENV{IPPROOT}/include)
add_executable(f_ipp_gradient f_ipp_gradient.cpp)
target_link_libraries(f_ipp_gradient ${BL_LIBRARIES} $ENV{IPPROOT}/lib/libippi.a $ENV{IPPROOT}/lib/libipps.a $ENV{IPPROOT}/lib/libippvm.a  $ENV{IPPROOT}/lib/libippcore.a )




