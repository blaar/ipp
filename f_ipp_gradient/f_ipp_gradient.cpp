//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include "ipp.h"

#include <unistd.h>
#include <math.h>

/* gradient using Schaar operator
 
 [  3  10  3 ]
 [  0   0  0 ]
 [ -3 -10 -3 ]
 
 */

int main(int argc, char **argv){
    blc_channel input, output, kernel, gradx, grady, norm, angle;
    int gradx_step, grady_step, norm_step, angle_step;
    char const  *norm_name, *angle_name, *input_name, *gradx_name, *grady_name;
    int input_width, input_height, border_size;
    
    int buffer_size;
    IppiSize output_ippi_size;
    Ipp8u *working_buffer;
    IppStatus ipp_error = ippStsNoErr;
    
    blc_program_set_description("Compute convolution 2d of the data");
    blc_program_add_option(&gradx_name, 'x', "gradx", "blc_channel-out", "gradx channel (optional)", NULL);
    blc_program_add_option(&grady_name, 'y', "grady", "blc_channel-out", "grady channel (optional)", NULL);
    blc_program_add_option(&norm_name, 'n', "norm", "blc_channel-out", "norm of the gradient (optional)", NULL);
    blc_program_add_option(&angle_name, 'a', "angle", "blc_channel-out", "angle of the gradient (optional)", NULL);
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "image on which to compute gradient", NULL);
    
    blc_program_init(&argc, &argv, blc_quit);
    
    if ((gradx_name || grady_name || norm_name || angle_name)==0)EXIT_ON_ERROR("You need to request at least one information (gradx, grady, norm, angle)");
    
    input.open(input_name, BLC_CHANNEL_READ);
    if (input.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&input, "It works only for dims_nb=2 for now");
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    input_width=input.dims[0].length;
    input_height=input.dims[1].length;
    
    
//    border_size=1; // Mask 3x3
    border_size=2; // Mask 5x5
    
    output_ippi_size.width=input_width-2*border_size; //mask
    output_ippi_size.height=input_height-2*border_size;
    
    if (gradx_name) {
        gradx.create_or_open(gradx_name, BLC_CHANNEL_WRITE, 'FL32', input.format, 2, output_ippi_size.width, output_ippi_size.height);
        gradx_step=gradx.dims[1].step;
        gradx.publish();
        blc_loop_try_add_waiting_semaphore(gradx.sem_ack_data);
        blc_loop_try_add_posting_semaphore(gradx.sem_new_data);
    }
    
    if (grady_name) {
        grady.create_or_open(grady_name, BLC_CHANNEL_WRITE, 'FL32', input.format, 2, output_ippi_size.width, output_ippi_size.height);
        grady_step=grady.dims[1].step;
        grady.publish();
        
        blc_loop_try_add_waiting_semaphore(grady.sem_ack_data);
        blc_loop_try_add_posting_semaphore(grady.sem_new_data);
    }
    if (norm_name) {
        norm.create_or_open(norm_name, BLC_CHANNEL_WRITE, 'FL32', input.format, 2, output_ippi_size.width, output_ippi_size.height);
        norm_step=norm.dims[1].step;

        norm.publish();
        blc_loop_try_add_waiting_semaphore(norm.sem_ack_data);
        blc_loop_try_add_posting_semaphore(norm.sem_new_data);
    }
    
    if (angle_name) {
        angle.create_or_open(angle_name, BLC_CHANNEL_WRITE, 'FL32', input.format, 2, output_ippi_size.width, output_ippi_size.height);
        angle_step=angle.dims[1].step;

        angle.publish();
        
        blc_loop_try_add_waiting_semaphore(angle.sem_ack_data);
        blc_loop_try_add_posting_semaphore(angle.sem_new_data);
    }
    
    
    ipp_error=ippInit();
    if (ipp_error!=ippStsNoErr) EXIT_ON_ERROR("ippInit error: '%d'", ipp_error);
    
    switch (input.type){
        case 'FL32':
        {
     //       ipp_error=ippiGradientVectorGetBufferSize(output_ippi_size, ippMskSize3x3, ipp32f, 1, &buffer_size);
            ipp_error=ippiGradientVectorGetBufferSize(output_ippi_size, ippMskSize5x5, ipp32f, 1, &buffer_size);

            if (ipp_error!=ippStsNoErr) EXIT_ON_ERROR("Ipp error '%d'", ipp_error);
            working_buffer = ippsMalloc_8u(buffer_size);
            
            BLC_COMMAND_LOOP(0){
       /*         ipp_error=ippiGradientVectorScharr_32f_C1R(input.floats+border_size+border_size*input.dims[0].length, input.dims[1].step, gradx.floats, gradx_step, grady.floats, grady_step,
                                                 norm.floats, norm_step, angle.floats, angle_step,
                                                 output_ippi_size, ippMskSize3x3, ippNormL1,
                                                 ippBorderInMem, -1,
                                                 working_buffer);*/
                ipp_error=ippiGradientVectorSobel_32f_C1R(input.floats+border_size+border_size*input.dims[0].length, input.dims[1].step, gradx.floats, gradx_step, grady.floats, grady_step,
                                                           norm.floats, norm_step, angle.floats, angle_step,
                                                           output_ippi_size, ippMskSize5x5, ippNormL1,
                                                           ippBorderInMem, -1,
                                                           working_buffer);
                if (ipp_error!=ippStsNoErr) EXIT_ON_ERROR("ippiGradientVectorSobel_32f_C1R error: '%d'", ipp_error);

            }
            ippsFree(working_buffer);
        }
            break;
        default:EXIT_ON_ARRAY_ERROR(&input, "Type not managed, only FL32 is");
    }
    
    
    return EXIT_SUCCESS;
}

