//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include "ipp.h"

#include <unistd.h>
#include <math.h>


#define DEFAULT_OUTPUT_NAME ":conv<pid>"


void ipp_infos()
{
    const IppLibraryVersion *lib;
    IppStatus status;
    Ipp64u mask, emask;
    
    /* Initialize Intel IPP library */
    /* Get Intel IPP library version info */
    lib = ippGetLibVersion();
    printf("%s %s\n", lib->Name, lib->Version);
    
    /* Get CPU features and features enabled with selected library level */
    status = ippGetCpuFeatures( &mask, 0 );
    if( ippStsNoErr == status ) {
        emask = ippGetEnabledCpuFeatures();
        printf("Features supported by CPU\tby Intel IPP\n");
        printf("-----------------------------------------\n");
        printf("  ippCPUID_MMX        = ");
        printf("%c\t%c\t",( mask & ippCPUID_MMX ) ? 'Y':'N',( emask & ippCPUID_MMX ) ? 'Y':'N');
        printf("Intel(R) architecture MMX(TM) technology supported\n");
        printf("  ippCPUID_SSE        = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSE ) ? 'Y':'N',( emask & ippCPUID_SSE ) ? 'Y':'N');
        printf("Intel(R) Streaming SIMD Extensions\n");
        printf("  ippCPUID_SSE2       = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSE2 ) ? 'Y':'N',( emask & ippCPUID_SSE2 ) ? 'Y':'N');
        printf("Intel(R) Streaming SIMD Extensions 2\n");
        printf("  ippCPUID_SSE3       = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSE3 ) ? 'Y':'N',( emask & ippCPUID_SSE3 ) ? 'Y':'N');
        printf("Intel(R) Streaming SIMD Extensions 3\n");
        printf("  ippCPUID_SSSE3      = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSSE3 ) ? 'Y':'N',( emask & ippCPUID_SSSE3 ) ? 'Y':'N');
        printf("Supplemental Streaming SIMD Extensions 3\n");
        printf("  ippCPUID_MOVBE      = ");
        printf("%c\t%c\t",( mask & ippCPUID_MOVBE ) ? 'Y':'N',( emask & ippCPUID_MOVBE ) ? 'Y':'N');
        printf("The processor supports MOVBE instruction\n");
        printf("  ippCPUID_SSE41      = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSE41 ) ? 'Y':'N',( emask & ippCPUID_SSE41 ) ? 'Y':'N');
        printf("Intel(R) Streaming SIMD Extensions 4.1\n");
        printf("  ippCPUID_SSE42      = ");
        printf("%c\t%c\t",( mask & ippCPUID_SSE42 ) ? 'Y':'N',( emask & ippCPUID_SSE42 ) ? 'Y':'N');
        printf("Intel(R) Streaming SIMD Extensions 4.2\n");
        printf("  ippCPUID_AVX        = ");
        printf("%c\t%c\t",( mask & ippCPUID_AVX ) ? 'Y':'N',( emask & ippCPUID_AVX ) ? 'Y':'N');
        printf("Intel(R) Advanced Vector Extensions instruction set\n");
        printf("  ippAVX_ENABLEDBYOS  = ");
        printf("%c\t%c\t",( mask & ippAVX_ENABLEDBYOS ) ? 'Y':'N',( emask & ippAVX_ENABLEDBYOS ) ? 'Y':'N');
        printf("The operating system supports Intel(R) AVX\n");
        printf("  ippCPUID_AES        = ");
        printf("%c\t%c\t",( mask & ippCPUID_AES ) ? 'Y':'N',( emask & ippCPUID_AES ) ? 'Y':'N');
        printf("AES instruction\n");
        printf("  ippCPUID_SHA        = ");
        printf("%c\t%c\t",( mask & ippCPUID_SHA ) ? 'Y':'N',( emask & ippCPUID_SHA ) ? 'Y':'N');
        printf("Intel(R) SHA new instructions\n");
        printf("  ippCPUID_CLMUL      = ");
        printf("%c\t%c\t",( mask & ippCPUID_CLMUL ) ? 'Y':'N',( emask & ippCPUID_CLMUL ) ? 'Y':'N');
        printf("PCLMULQDQ instruction\n");
        printf("  ippCPUID_RDRAND     = ");
        printf("%c\t%c\t",( mask & ippCPUID_RDRAND ) ? 'Y':'N',( emask & ippCPUID_RDRAND ) ? 'Y':'N');
        printf("Read Random Number instructions\n");
        printf("  ippCPUID_F16C       = ");
        printf("%c\t%c\t",( mask & ippCPUID_F16C ) ? 'Y':'N',( emask & ippCPUID_F16C ) ? 'Y':'N');
        printf("Float16 instructions\n");
        printf("  ippCPUID_AVX2       = ");
        printf("%c\t%c\t",( mask & ippCPUID_AVX2 ) ? 'Y':'N',( emask & ippCPUID_AVX2 ) ? 'Y':'N');
        printf("Intel(R) Advanced Vector Extensions 2 instruction set\n");
        printf("  ippCPUID_AVX512F    = ");
        printf("%c\t%c\t",( mask & ippCPUID_AVX512F ) ? 'Y':'N',( emask & ippCPUID_AVX512F ) ? 'Y':'N');
        printf("Intel(R) Advanced Vector Extensions 3.1 instruction set\n");
        printf("  ippCPUID_AVX512CD   = ");
        printf("%c\t%c\t",( mask & ippCPUID_AVX512CD ) ? 'Y':'N',( emask & ippCPUID_AVX512CD ) ? 'Y':'N');
        printf("Intel(R) Advanced Vector Extensions CD (Conflict Detection) instruction set\n");
        printf("  ippCPUID_AVX512ER   = ");
        printf("%c\t%c\t",( mask & ippCPUID_AVX512ER ) ? 'Y':'N',( emask & ippCPUID_AVX512ER ) ? 'Y':'N');
        printf("Intel(R) Advanced Vector Extensions ER instruction set\n");
        printf("  ippCPUID_ADCOX      = ");
        printf("%c\t%c\t",( mask & ippCPUID_ADCOX ) ? 'Y':'N',( emask & ippCPUID_ADCOX ) ? 'Y':'N');
        printf("ADCX and ADOX instructions\n");
        printf("  ippCPUID_RDSEED     = ");
        printf("%c\t%c\t",( mask & ippCPUID_RDSEED ) ? 'Y':'N',( emask & ippCPUID_RDSEED ) ? 'Y':'N');
        printf("The RDSEED instruction\n");
        printf("  ippCPUID_PREFETCHW  = ");
        printf("%c\t%c\t",( mask & ippCPUID_PREFETCHW ) ? 'Y':'N',( emask & ippCPUID_PREFETCHW ) ? 'Y':'N');
        printf("The PREFETCHW instruction\n");
        printf("  ippCPUID_KNC        = ");
        printf("%c\t%c\t",( mask & ippCPUID_KNC ) ? 'Y':'N',( emask & ippCPUID_KNC ) ? 'Y':'N');
        printf("Intel(R) Xeon Phi(TM) Coprocessor instruction set\n");
    }
}

template <typename O_t, typename D, typename K> void convolute(O_t *output, int o_width, int o_height, D *input, int iwidth, int iheight, K* kernel, int kwidth, int kheight){
    int i, j, ki, kj, divisor=1;
    O_t value;
    IppStatus status = ippStsNoErr;
    Ipp32f* pSrc1 = NULL, *pSrc2 = NULL, *pDst = NULL; /* Pointers to source/destination images */
    int srcStep1 = 4*iwidth, srcStep2 = 4*kwidth, dstStep = 4*o_width;       /* Steps, in bytes, through the source/destination images */
    IppiSize dstSize  = { o_width, o_height };     /* Size of destination ROI in pixels */
    IppiSize src1Size = { iwidth, iheight };     /* Size of destination ROI in pixels */
    IppiSize src2Size = { kwidth, kheight }; /* Size of destination ROI in pixels */
    Ipp8u *pBuffer = NULL;  /* Pointer to the work buffer */
    int iTmpBufSize = 0;    /* Common work buffer size */
    int numChannels = 1;
    IppEnum funCfgFull = (IppEnum)(ippAlgAuto | ippiROIValid | ippiNormNone);

    
    status = ippiConvGetBufferSize(src1Size, src2Size, ipp32f, numChannels, funCfgFull, &iTmpBufSize);
    if (status!=ippStsNoErr) EXIT_ON_ERROR("ippiConvGetBufferSize error %d", status);
    pBuffer = ippsMalloc_8u(iTmpBufSize);
    
    BLC_COMMAND_LOOP(0){

    status = ippiConv_32f_C1R(input, srcStep1, src1Size, kernel, srcStep2, src2Size, output, dstStep, funCfgFull, pBuffer);
    if (status!=ippStsNoErr) EXIT_ON_ERROR("ippiConv_32f_C1R error '%d'", status);
    }
}

int main(int argc, char **argv){
    blc_channel input, output, kernel;
    char const  *output_name, *input_name, *kernel_name, *infos;
    int output_width, output_height;
    int kwidth, kheight, iwidth, iheight;
    uint32_t output_type;
    
    blc_program_set_description("Compute convolution 2d of the data");
    blc_program_add_option(&infos, 'i', "infos", NULL, "display CPU information", NULL);
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "data to convolute ", NULL);
    blc_program_add_parameter(&kernel_name, "blc_channel-in", 1, "kernel to use for convolution ", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    
    
    ippInit();

    if (infos) {
        ipp_infos();
        return EXIT_SUCCESS;
    }
    
    input.open(input_name, BLC_CHANNEL_READ);
    if (input.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&input, "It works only in 2D for now");
    iwidth=input.dims[0].length;
    iheight=input.dims[1].length;
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    kernel.open(kernel_name, BLC_CHANNEL_READ);
    if (kernel.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&kernel, "It works only in 2D for now");

    kwidth=kernel.dims[0].length;
    kheight=kernel.dims[1].length;
    
    if ((iwidth < kwidth) || (iheight < kheight)) EXIT_ON_ERROR("Kernel cannot be smaller than input");
    blc_loop_try_add_waiting_semaphore(kernel.sem_new_data);
    blc_loop_try_add_posting_semaphore(kernel.sem_ack_data);
    
    if (input.type=='FL32' ||  kernel.type=='FL32') output_type='FL32';
    else output_type='UIN8';
    
    if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name,":conv%d", getpid()), -1, NULL);
    
    output_width=iwidth-kwidth+1;
    output_height=iheight-kheight+1;
    output.create_or_open(output_name, BLC_CHANNEL_WRITE, output_type, input.format, 2, output_width, output_height);
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    output.publish();

    switch (input.type){
        case 'UIN8':
            switch (kernel.type){
             /*   case 'UIN8':
                    convolute(output.uchars, output_width, output_height, input.uchars, iwidth, iheight, kernel.uchars, kwidth, kheight);
                    break;
                case 'FL32':
                    convolute(output.floats, output_width, output_height, input.uchars, iwidth, iheight, kernel.floats, kwidth, kheight);
                    break;
            */    default:EXIT_ON_ARRAY_ERROR(&kernel, "Type kernel is not managed  for input 'UIN8', only UIN8|FL32 are");
                    
            }
            break;
        case 'FL32':
            switch (kernel.type){
               /* case 'UIN8':
                    convolute(output.floats, output_width, output_height, input.floats, iwidth, iheight, kernel.uchars, kwidth, kheight);
                    break;
                */case 'FL32':
                    convolute(output.floats, output_width, output_height, input.floats, iwidth, iheight, kernel.floats, kwidth, kheight);
                    break;
                default:EXIT_ON_ARRAY_ERROR(&kernel, "Type kernel is not managed  for input 'FL32', only UIN8|FL32 are");
                    
            }
            break;
        default:EXIT_ON_ARRAY_ERROR(&input, "Type not managed, only UIN8|FL32 are");
    }
    return EXIT_SUCCESS;
}

